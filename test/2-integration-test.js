var expect = require('chai').expect;
var flow = require('../lib/flow');

var success = {
    available: false,
    zip: 30024
};

var fail = {
    error: 'Internal Server Error.'
};

describe('Testing Flow Lib', () => {
    it('Check flow of all the methods', done => {
        flow.endToEnd(success.zip).then(result => {
            expect(result.available).to.be.equal(success.available);
            expect(result.zip).to.be.equal(success.zip);
            done();
        }, error => {
            done(err);
        });
    }).timeout(10000);
});
