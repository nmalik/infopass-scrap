var expect = require('chai').expect;
var request = require('request');
var config = require('../config');

var success = {
    available: false,
    zip: '30024'
};

describe('Testing Lambda method', () => {
    it('Calling Lambda', done => {
        request.get({
            url: config.lambda + success.zip
        }, (err, resp, body) => {
            var result = JSON.parse(body);
            expect(result.available).to.be.equal(success.available);
            expect(result.zip).to.be.equal(success.zip);
            done();
        });
    }).timeout(10000);
});
