var expect = require('chai').expect;
var method = require('../lib/method');
var request = require('request');

var req = request.defaults({
    jar: true,
    rejectUnauthorized: false,
    followAllRedirects: true
});

describe('Testing Methods Lib', () => {
    describe('Testing class structure', () => {
        it('Check if getInfopassHome exists', () => {expect(method.getInfopassHome).to.be.a('function');});
        it('Check if getFieldOfficeForm exists', () => { expect(method.getFieldOfficeForm).to.be.a('function'); });
        it('Check if checkOpenAppointment exists', () => {expect(method.checkOpenAppointment).to.be.a('function');});
        it('Check if extractForm exists', () => {expect(method.extractForm).to.be.a('function');});
        it('Check if extractOpenWeeks exists', () => {expect(method.extractOpenWeeks).to.be.a('function');});
    });

    describe('Testing Method calls', () => {
        it('Check getInfopassHome call', done => {
            method.getInfopassHome(req).then(result => {
                expect(result).to.be.equal(true);
                done();
            }, error => {
                expect(error).to.be.equal(false);
                done();
            }).catch(err => {
                done(err);
            });
        });

        it('Check getFieldOfficeForm call', done => {
            method.getFieldOfficeForm(req).then(result => {
                expect(result).to.be.equal(''); 
                done();
            }, error => {
                expect(error).to.be.equal(null);
                done();
            }).catch(err => {
                done(err);
            });
        });

        it('Check checkOpenAppointment call', done => {
            method.checkOpenAppointment(req, null).then(result => {
                expect(result).to.be.equal(0); 
                done();
            }, error => {
                expect(error).to.be.equal(null);
                done();
            }).catch(err => {
                done(err);
            });
        });

        it('Check extractForm call', done => {
            var result = method.extractForm('');
            expect(result).to.be.equal('');
            done();
        });

        it('Check extractOpenWeeks call', done => {
            var result = method.extractOpenWeeks('');
            expect(result).to.be.equal(0);
            done();
        });
    });
});