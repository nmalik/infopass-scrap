module.exports = {
    infopass : {
        step1 : "https://my.uscis.gov/en/appointment/new?appointment%5Binternational%5D=false",
        step2 : "https://my.uscis.gov/en/appointment/field_offices?appointments_appointment%5Bzip%5D=%s",
        step3 : "https://my.uscis.gov/en/appointment/dates"
    },
    lambda: 'https://o6joum7htj.execute-api.us-east-1.amazonaws.com/default/inforpass-v2?zip='
};