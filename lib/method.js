var config = require('../config');
var cheerio = require('cheerio');

var getInfopassHome = (req) => {
    return new Promise((resolve, reject) => {
        req.get({
            url: config.infopass.step1
        }, function(err, resp, body){
            if(!err){
                if(resp.status === 200) resolve(true);
                else reject(false);
            }else{
                reject(false);
            }
        });
    });
}

var getFieldOfficeForm = (req) => {
    return new Promise((resolve, reject) => {
        req.get({
            url: config.infopass.step2
        }, function(err, resp, body){
            if(!err){
                var form = extractForm(body);
                resolve(form);
            }else{
                reject(null);
            }
        });
    });
}

var checkOpenAppointment = (req, form) => {
    return new Promise((resolve, reject) => {
        req.post({
            url: config.infopass.step3,
            form: form
        }, function(err, resp, body){
            if(!err){
                var weeks = extractOpenWeeks(body);
                resolve(weeks);
            }else{
                reject();
            }
    
        });
    });
}

var extractForm = (body) => {
    var $ = cheerio.load(body);
    var form = $('.field-office-form form').serialize();
    return form;
}

var extractOpenWeeks = (body) => {
    var $ = cheerio.load(body);
    var weeks = $('.calendar > .uscis-row > .months').length;
    return weeks;
}

module.exports = {
    getInfopassHome: getInfopassHome,
    getFieldOfficeForm: getFieldOfficeForm,
    checkOpenAppointment: checkOpenAppointment,
    extractForm: extractForm,
    extractOpenWeeks: extractOpenWeeks
}