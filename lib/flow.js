var async = require('async');
var request = require('request');
var util = require('util');
var method = require('../lib/method');

var req = request.defaults({
    jar: true,
    rejectUnauthorized: false,
    followAllRedirects: true
});

var endToEnd = zip => {
    return new Promise((resolve, reject) => {
        async.waterfall([getInfopassHomeWrapper, getFieldOfficeFormWrapper, checkOpenAppointmentWrapper], (err, result) => {
            if(!err){
                if(result > 0){
                    resolve({
                        available: true, 
                        weeks: result,
                        zip: zip
                    });
                }else{
                    resolve({
                        available: false,
                        zip: zip
                    });
                }
            }else{
                resolve({
                    error: err
                });
            }
        });
    });
};

var getInfopassHomeWrapper = callback => {
    method.getInfopassHome(req)
    .then(result => callback(null, result), error => callback(error));
}

var getFieldOfficeFormWrapper = callback => {
    method.getFieldOfficeForm(req)
    .then(result => callback(null, result), error => callback(error));
}

var checkOpenAppointmentWrapper = (result, callback) => {
    method.checkOpenAppointment(req, result)
    .then(result => callback(null, result), error => callback(error));
}

module.exports = {
    endToEnd: endToEnd
};
