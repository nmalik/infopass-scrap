var flow = require('./lib/flow');

exports.handler = function(event, context, callback){
    var zip = event.queryStringParameters.zip || 30005; //Just setting a defult zip
    flow.endToEnd(zip).then(result => {
        var response = {
            statusCode: 200,
            body: JSON.stringify(result)
        };
        callback(null, response);
    }, err => {
        var response = {
            statusCode: 500,
            body: 'Internal Server Error. Contact Admin.'
        };
    });
}

